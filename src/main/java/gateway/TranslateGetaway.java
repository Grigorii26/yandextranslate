package gateway;

import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import entities.Response;


public class TranslateGetaway {

    private static final String URL = "https://translate.api.cloud.yandex.net/translate/v2/translate";
    private static final String TOKEN = "t1.9euelZqMyI_LyJyNlsiUmpPNx46ble3rnpWayo6XloqcjpLLkpzGkIqMlYrl8_dJdx15-e83SBlV_t3z9wkmG3n57zdIGVX-.GXQJSJmZ_nLBDqMPwiaCdOLQ2L9oyO0bcjrfh5yKp9kPDOUrN1_AdvrsV7OSMQnRcLsMLK8uQwsU5BgDHPkcDg";

    public Response getTranslate(String text) throws UnirestException {
        Gson gson = new Gson();
        HttpResponse<String> response = Unirest.post(URL)
                .header("Accept", "*/*")
                .header("Authorization", "Bearer " + TOKEN)
                .body("{" +
                        "  \"folderId\": \"b1ggm17f3filtfg7cbhg\",\n" +
                        "  \"texts\": [\"" + text + "\"],\n" +
                        "  \"targetLanguageCode\": \"ru\"\n" +
                        "}")
                .asString();
        String strResponse = response.getBody();
        System.out.println("response: "+strResponse);
        return gson.fromJson(strResponse, Response.class);
    }


}

