package entities;

import com.google.gson.annotations.SerializedName;


public class Translation {
    @SerializedName("text")
    private String text;

    public String getText() {
        return text;
    }

}
