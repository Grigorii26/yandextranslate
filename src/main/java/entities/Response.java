package entities;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class Response {
    @SerializedName("translations")
    List<Translation> translation = new ArrayList<Translation>();

    public List<Translation> getTranslation() {
        return translation;
    }

}
