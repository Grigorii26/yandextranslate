package gateway;

import com.mashape.unirest.http.exceptions.UnirestException;
import entities.Response;
import entities.Translation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestTranslateYandex {

    @Test
    public void shouldTranslate() throws UnirestException {
        TranslateGetaway translateGetaway = new TranslateGetaway();
        Response translate = translateGetaway.getTranslate("Hello World!");
        String result = translate.getTranslation().stream()
                .findFirst()
                .map(Translation::getText)
                .orElseThrow();
        assertEquals("���� ������!", result);


    }

}
